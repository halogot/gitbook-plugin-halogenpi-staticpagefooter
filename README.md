# gitbook-plugin-halogenpi-staticpagefooter

This plugin is to show the creation date and word count as footer

The general purpose is to show a footer by page name

* Config

Gitbook book.json

```javascript
    "plugins": [
        "halogenpi-staticpagefooter"
    ],
    "pluginsConfig": {
        "halogenpi-staticpagefooter": {
            "wordcount":"%d words",
            "modify_label": "Last edited on:",
            "staticmodifies": {"page1.md": "2000-01-01",
                "page2.md": "2000-01-02"}
        }
```

copyright and modify_label support html code

wordcount is a formatter for showing number of words

cjk is supported

If page path is not found in staticmodifies, it does not show the right footer by default
