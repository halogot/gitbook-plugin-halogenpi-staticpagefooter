module.exports = {
  book: {
    assets: './assets',
    css: [
      'footer.css'
    ],
  },
  hooks: {
    'page:before': function(page) {
      var _label = 'File Modify: ',
          _wordcount = '%d words';
	  var _modifies = {};
      if(this.options.pluginsConfig['halogenpi-staticpagefooter']) {
        _label = this.options.pluginsConfig['halogenpi-staticpagefooter']['modify_label'] || _label;
		_modifies = this.options.pluginsConfig['halogenpi-staticpagefooter']['staticmodifies'] || _modifies;
        _wordcount = this.options.pluginsConfig['halogenpi-staticpagefooter']['wordcount'] || _wordcount;
      }
	  var _filepath = page.path;
	  var _filemodifytxt = _modifies[_filepath];
	  if (typeof _filemodifytxt !== 'undefined') {
		  _filemodifytxt = _label + _filemodifytxt;
	  } else {
		  _filemodifytxt = "";
	  }
      var pattern = /[a-zA-Z0-9_\u0392-\u03c9\u00c0-\u00ff\u0600-\u06ff]+|[\u4e00-\u9fff\u3400-\u4dbf\uf900-\ufaff\u3040-\u309f\uac00-\ud7af]+/g;
	  var m = page.content.match(pattern);
	  var count = 0;
	  if (m) {
		  for (var i = 0; i < m.length; i++) {
			  if (m[i].charCodeAt(0) >= 0x4e00) {
				  count += m[i].length;
			  } else {
				  count += 1;
			  }
		  }
	  }
	  _wordcount = _wordcount.replace(/%d/, count + '');
      var _wordcount = '<span class="copyright">'+_wordcount+'</span>';
	  var str = ' \n\n<footer class="page-footer">' + _wordcount +
		'<span class="footer-modification">' +
		_filemodifytxt +
		'\n</span></footer>';
	  page.content = page.content + str;
      return page;
    }
  },
  filters: {}
};
